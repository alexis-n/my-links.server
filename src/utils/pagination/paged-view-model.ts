import { Paged } from './paged';

export class PagedViewModel<T, S> {
  public values: T[];
  public total: number;
  public skipped: number;
  public count: number;
  public page: {
    current: number;
    total: number;
  };

  constructor(
    page: Paged<S>,
    ctor: (new (val: S) => T) | (new (...args: any[]) => T),
    count: number,
    skipped: number,
  ) {
    this.values = page.data.map((data) =>
      data instanceof ctor ? data : new ctor(data),
    );
    this.total = page.total;
    this.skipped = skipped;
    this.count = page.count;

    this.page = {
      current: Math.floor(skipped / count),
      total: Math.ceil(page.total / count),
    };
  }
}
