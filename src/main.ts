import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({ origin: '*' });
  const port = process.env.PORT || 3000;

  app
    .listen(port)
    .then(() => console.log(`🚀 Serveur running on port ${port} 🚀`));
}
bootstrap();
